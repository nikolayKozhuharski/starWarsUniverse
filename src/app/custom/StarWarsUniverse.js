import Entity from "./Entity";

export default class StarWarsUniverse {
  constructor() {
    this.entities = [];
  }
  async init() {
    let data = [];
    const response = await fetch("https://swapi.booost.bg/api/");
    data = await response.json();
    for (const key in data) {
      fetch(data[key]).then((data) => data.json()).then((data) => {
        let element = new Entity(key, data);
        this.entities.push(element);
      });
    }
    return data;
  }
}
